# build
Dans cette section se trouvent les fichiers qui servent à faire nos "builds".
Vous y trouverez des Dockerfile qui servent à produire des images de conteneurs. On pourrait y ajouter aussi des makefiles ou autres fichiers servant à compiler des applications.

## Conteneurs

Nous avons l'habitude de construire les images docker avec la commande suivante:
```bash
docker build -t mon_image:latest .
```

Or dans ce cas nous avons le Dockerfile dans le dossier *build* et les sources de l'application dans le dossier *src* pour faciliter la lecture des sources et standardiser l'arborescence.

En utilisant la commande suivante vous pourrez construire une image en spécifiant le contexte:

Cette commande doit être exécutée depuis la racine du dépôt (xxx/outil-exemples dans notre cas)
```bash
docker build -t mon_image:latest -f build/Dockerfile src
```

### Intégration dans le pipeline

Le pipeline par défaut va chercher s'il existe un fichier *Dockerfile* dans le répertoire *build* et lancer la commande de build en fonction.
La localisation du *Dockerfile* est importante car les chemins sont codés en dur dans le Jenkinsfile. Il faudra sortir du standard pour changer ce paramètre.