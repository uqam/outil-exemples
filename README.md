# outil-exemples

Dépôt d'exemples pour les employés de l'UQAM.

Ce dépôt rassemble des exemples et des bonnes pratiques sur les plateformes et outils utilisés à l'UQAM. (Git, Jenkins, Kubernetes, AWS, php, nodejs, .net, python, etc.)

Inspirez-vous, aidez à l'améliorer, participez, commentez.

## Organisation du dépôt

Afin de permettre à tous les automatismes et scripts de fonctionner sans plus d'adaptation il est fortement recommandé de suivre l'arborescence suivante. Elle permettra également à tous les projets la respectant d'être plus accessible aux nouveaux contributeurs.

Le contenu est présenté à titre indicatif, si le projet ne nécessite pas de *build*, il n'est pas obligatoire de créer un dossier build vide.

### Arborescence

#### src

Contient les sources de l'application.

#### build

Contient le(s) Dockerfile et outils nécessaires pour le *build* de votre application.

  * Fichiers attendus:
    - Dockerfile
    - Dockerfile.xxx
    - Makefile
    - ...

#### deploy

Contient les fichiers permettant de déployer l'application.

  * Fichiers attendus:
    - manifests Kubernetes
    - Templates Helm
    - Templates cloudformation
    - ...

#### pipeline

Contient le pipeline, et ses dépendances.

  * Fichiers attendus:
    - Jenkinsfile
    - Jenkins.params
    - ...

### Branches

Conformément à la [norme sur les nomenclatures](https://wiki.uqam.ca/pages/viewpage.action?pageId=67421695), les branches principales sont nommées **dev**, **test**, **preprod**, **prod**.
Bien entendu il est possible de créer des branches avec d'autres noms afin de suivre votre *workflow* de travail, ex: *[Git Flow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)*

#### Travailler avec Gitflow

Illustration d'une technique pour découpler le nom des branches du nom des fichiers de configuration Kubernetes. Permet d'utiliser des branches "Gitflow" du type 'release/test'. Également, utilisation de conditionnels dans les étapes Jenkins pour limiter la production d'images Docker et le déploiement Kubernetes aux seules branches pour lesquelles on désire un environnement.

=> Exemple : https://bitbucket.org/uqam/infra-sdn-api/src/master/pipeline.

## Contributions & *Workflow* de travail

Partagez vous meilleures pratiques et améliorez celles qui sont référencées ici.

### Comment faire?
  * Commencez par créez un billet Jira dans le projet [INFRA](https://jira.uqam.ca/secure/RapidBoard.jspa?projectKey=INFRA&rapidView=130&view=planning)
  * Clonez le dépôt en local
  * Créez une branche portant le nom du billet Jira

## Documentation

D'autres readme sont présents dans les autres sections du dépôts afin de donner plus de détails sur les éléments concernés à ce niveau de l'arboresence.