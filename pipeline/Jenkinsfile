// Déclaration d'un pipeline
pipeline {
  /*
    Déclaration de l'agent qui sera utilisé pour exécuter le pipeline.
    À l'UQAM nous n'utilisons pas d'agent autre que le master pour l'instant.
    https://www.jenkins.io/doc/book/pipeline/syntax/#agent
  */
  agent {
    node {
      label 'master'
    }
  }

  /*
    Options des pipelines
    https://www.jenkins.io/doc/book/pipeline/syntax/#options
  */
  options {
    /*
      Ne pas autoriser les build concurrents,
      si plusieurs push sont faits dans un court labs de temps,
      les builds seront exécutés séquentiellement.
    */
    disableConcurrentBuilds()
  }

  /*
    Variables permettant de configurer globalement le nom des branches afin de les réutiliser dans le pipeline
  */
  environment {
    BRANCH_DEV='dev'
    BRANCH_PROD='prod'
  }

  /*
    On entre ici dans la déclaration des différentes "stages"
    https://www.jenkins.io/doc/book/pipeline/syntax/#stages
  */
  stages {

    /*
      Stage Optionnel: Récupération des mots de passes et autres secrets contenus dans la voute
      https://www.jenkins.io/doc/book/pipeline/syntax/#stage
    */
    stage('Fetching credentials') {

      /*
        Condition: On utilise la balise "when" pour spécifier que l'exécution de ce stage est conditionnelle
        https://www.jenkins.io/doc/book/pipeline/syntax/#when
      */
      when {
        /*
          Condition anyOf: Si une de ces conditions est vérifiée alors le stage est exécuté
          Ici on vérifie que les variables nécessaire pour obtenir un secret via la voute son non nulles avant d'exécuter cette étape
        */
        anyOf {
          expression{
            env.credentials != null &&
            env.passwordlistid != null &&
            env.passwordtitle != null &&
            env.passwordfield != null
          }
        }
      }

      /*
        Steps: C'est ici qu'on décrit les actions à faire pour ce stage
        https://www.jenkins.io/doc/book/pipeline/syntax/#steps
      */
      steps{
        // Ici on charge le fichier de paramètres de Jenkins
        load("pipeline/Jenkins.params")

        /*
        withCredentials est un plugin qui permet de charger des secrets en variables d'environnement
        pour pouvoir les utiliser dans le code qui est encapsulé entre ses balises.
        Les secrets chargés via ce plugin apparaîtrons masqués **** dans la console de logs de Jenkins. 
        */
        withCredentials([
          /*
          Ici le credential Binding utilisé est de type 'string'.
          Voir ici pour une liste des bindings: https://www.jenkins.io/doc/pipeline/steps/credentials-binding/
          */
          string(credentialsId:env.credentials, variable: 'TOKEN')
          ]){
          // Les balises script servent à écrire du code
          script{
            def response = sh(script: "curl -H \"apikey: $TOKEN\" -H \"reason:Jenkins Build\" https://voute.uqam.ca:9119/api/searchpasswords/${env.passwordlistid}?title=${env.passwordtitle}", returnStdout: true)
            def parsedResponse = readJSON text: response
            ssh_key_public=parsedResponse[0].GenericField1
            ssh_key_private=parsedResponse[0].GenericField2
          }
        }
      }
    }

    stage('Build and upload container image') {
      /*
        Info: Ici la condition est intéressante car elle permet de ne pas construire l'image du conteneur à chaque exécution du pipeline.
      */
      when {
        anyOf {
          changeset "build/Dockerfile"
          changeset "src/**"
          changeset "pipeline/**"
          triggeredBy 'UserIdCause' // Pour exécuter ce stage lors d'un build déclenché manuellement
        }
      }
      steps {
        // Ici on charge le fichier de paramètres de Jenkins
        load "pipeline/Jenkins.params"
        /*
        withCredentials permet de charger des secrets en variables d'environnement
        pour pouvoir les utiliser dans le code qui est encapsulé entre ses balises.
        */
        withCredentials(
          [
            dockerCert(credentialsId: 'DockerHost-infra-build-dev-01.si.uqam.ca', variable: 'DOCKER_CERT_PATH'),
            [$class: 'UsernamePasswordMultiBinding', credentialsId: 'Docker', usernameVariable: 'DOCKERHUB_USERNAME', passwordVariable: 'DOCKERHUB_PASSWORD']
          ]) {
          // On définit un alias de commande docker DOCKER_CMD pour simplifier la lecture par la suite.
          script {
            DOCKER_CMD = sh(script: "echo docker -H ${env.dockerhostbuilder} --tls --tlscacert=${DOCKER_CERT_PATH}/ca.pem --tlscert=${DOCKER_CERT_PATH}/cert.pem --tlskey=${DOCKER_CERT_PATH}/key.pem", returnStdout: true).trim()
          }

          print '** Build image **'
          script {
            if (env.BRANCH_NAME == env.BRANCH_PROD) {
              sh "${DOCKER_CMD} build -t ${env.registry}:${env.tag} -f build/Dockerfile --build-arg PROD_FLAG='--production' ."
            } else {
              sh "${DOCKER_CMD} build -t ${env.registry}:${env.tag} -f build/Dockerfile ."
            }
          }
          print '******************'
          print ''
          print '** Upload image **'
          script {
            /*
            Ici les variables USERNAME et PASSWORD sont celles qui ont été chargées dans la balise "withCredentials",
            elles seront masquées dans la console Jenkins
            */
            sh "${DOCKER_CMD} login -u ${DOCKERHUB_USERNAME} -p ${DOCKERHUB_PASSWORD}"
            sh "${DOCKER_CMD} push ${env.registry}:${env.tag}"
            sh "${DOCKER_CMD} logout"
          }
          print '******************'
        }
      }
    }

    stage('Deploy on Kubernetes') {
      /*
      On utilise la condition when combinée avec anyOf pour ne déployer que les branches pour lesquelles on a préparé un environnement Kubernetes
      */
      when {
        anyOf {
          branch BRANCH_DEV
          branch BRANCH_PROD
          triggeredBy 'UserIdCause' // Pour exécuter ce stage lors d'un build déclenché manuellement
        }
      }
      steps {
        load "pipeline/Jenkins.params"
        withCredentials(
          [
            // Certificat pour s'authentifier sur le serveur de build:
            dockerCert(credentialsId: 'DockerHost-Chaland', variable: 'DOCKER_CERT_PATH'),
            // Authentification sur le cluster Kubernetes:
            kubeconfigContent(credentialsId: "${env.kubeconfig}", variable: 'KUBECONFIG_CONTENT'),
            /* Authentification pour le registre dockerhub (nécessaire pour la limitation sur les pulls)
              #DockerHub */
            [$class: 'UsernamePasswordMultiBinding', credentialsId: 'Docker', usernameVariable: 'DOCKERHUB_USERNAME', passwordVariable: 'DOCKERHUB_PASSWORD']
          ]
        ) {
          print 'Preparation du kubeconfig...'
          script {
            // Préparation d'une structure de dossiers contenant le kubeconfig pour le copier dans le conteneur distant sur Chaland
            sh "rm -rf kube && mkdir kube && echo \"${KUBECONFIG_CONTENT}\" > kube/config"

            // On définit un alias de commande docker DOCKER_CMD pour simplifier la lecture par la suite.
            DOCKER_CMD = sh(script: "echo docker -H chaland.si.uqam.ca --tls --tlscacert=${DOCKER_CERT_PATH}/ca.pem --tlscert=${DOCKER_CERT_PATH}/cert.pem --tlskey=${DOCKER_CERT_PATH}/key.pem", returnStdout: true).trim()

            // Ici on crée un nom de conteneur qui sera unique et identifiable pour éviter les conflits lorsqu'on va faire des builds à répétition
            CONTAINER_NAME = "\"${env.appname}\"-\"${env.BRANCH_NAME}\"-\"${env.BUILD_NUMBER}\""

            // On lance l'image docker contenant les binaires kubectl et helm à la version désirée
            sh "${DOCKER_CMD} run --name ${CONTAINER_NAME} --rm -d alpine/k8s:1.14.9 sleep 180"

            // C'est là qu'on copie le dossier contenant le kubeconfig
            sh "${DOCKER_CMD} cp kube ${CONTAINER_NAME}:/root/.kube"

            // Nettoyage
            sh "rm -rf kube"

            // On copie aussi les fichiers Helm qui seront utilisés pour le déploiement
            sh "${DOCKER_CMD} cp deploy/kubernetes ${CONTAINER_NAME}:/root/deploy"

            /*
              Création du secret dockerhub pour se connecter au registre
              #DockerHub
             */
            sh "${DOCKER_CMD} exec ${CONTAINER_NAME} kubectl --namespace ${env.namespace} get secret dockerhub || \
                ${DOCKER_CMD} exec ${CONTAINER_NAME} kubectl --namespace ${env.namespace} create secret docker-registry dockerhub --docker-username=${DOCKERHUB_USERNAME} --docker-password=${DOCKERHUB_PASSWORD}"

            /*
              Ici soit un déploie, soit on met à jour.
              Si la commande de déploiement échoue, on considère qu'il y a déjà une instance qui roule et on lance la commande de mise à jour.
            */
            sh "${DOCKER_CMD} exec ${CONTAINER_NAME} helm upgrade -i -f /root/deploy/values.yaml -f /root/deploy/${env.BRANCH_NAME}.yaml --namespace=${env.namespace} ${env.appname} /root/deploy"

            sh "${DOCKER_CMD} stop ${CONTAINER_NAME}"
          }
        }
      }
    }
  }
}
