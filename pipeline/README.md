# pipeline
Dans cette section se trouvent les fichiers qui décrivent le pipeline et ses paramètres.

## Jenkinsfile
Le fichier Jenkinsfile décrit les étapes du pipeline à exécuter. Il est écrit en groovy.

Le Jenkinsfile de ce dépôt est agrémenté de nombreux commentaires pour en faciliter la compréhension.
Il contient beaucoup d'exemple usuels qui sont facilement réutilisables dans votre dépôt.

## Jenkins.params
Ce fichier permet de dissocier le paramétrage du pipeline avec sa déclaration.
Ainsi dans la majorité des cas le pipeline n'a pas besoin d'être adapté aux cas de figure spécifiques, le configuration des variables dans ce fichier est suffisante.

## Todo
* Utiliser les templates pour rendre le pipeline encore plus modulaire et réutilisable:
  * https://www.jenkins.io/blog/2017/10/02/pipeline-templates-with-shared-libraries/
  * https://www.jenkins.io/doc/book/pipeline/shared-libraries/