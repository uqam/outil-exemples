# deploy

Ici se trouvent les fichiers qui servent à déployer une application.
À l'heure de l'écriture de ces lignes, le dossier contient un template HELM qui sert à déployer des applications sur Kubernetes.

[TOC]

## Déploiements avec HELM

Les templates Helm sont pratiques pour déployer des manifests Kubernetes dans différentes environnements en changeant seulement certains paramètres.

Les fichiers templates contenus dans *templates* sont des manifests kubernetes dans lesquels on a changé les valeurs par des variables cd la forme *{{ .Values.myVariable }}*


## Configurer l'authentification aux dépôts

Suite à l'imposition de limites pour les *pull* d'images sur le registre hub.docker.com, il est devenu très important de s'authentifier pour récupérer les images afin de bénéficier d'un quota de 200 *pulls* / 6 heures.

### AWS ECR

#### Configuratoin du pipeline
Voir le [README.md](aws/ecr/#markdown-header-usage-et-configuration) section *Usage et configuration*

#### Configuration des manifests Kubernetes


### DockerHub
#### Création du *secret* dockerhub

* Créer un secret *dockerhub* comme suit dans votre namespace:

```bash
kubectl --namespace <monnamespace> create secret docker-registry dockerhub --docker-username=<monuser> --docker-password=<monpassword>
```

**Notes**:

  - Le *stage* 'Deploy on Kubernetes' de ce dépôt (outil-exemples) contient les instructions pour intégrer la création du *secret* dans votre *pipeline*. Les identifiants pour *DockerHub* sont déjà configurés dans Jenkins. Chercher *#DockerHub* pour trouver les instructions correspondantes

  - Le mot de passe pour le compte *docker hub uqam* se trouve dans la voute. Si vous n'y avez pas accès vous pouvez en faire la demande à la sécurité ou demander un partage à l'équipe Infra sur le [groupe UQAM Kubernetes](https://teams.microsoft.com/l/team/19%3aa2a73aa639734ac393e9e333b1679f49%40thread.skype/conversations?groupId=6303579e-2906-4954-b322-7aa6ad2c0c90&tenantId=12cb4e1a-42da-491c-90e1-7a7a9753506f)

#### Modification du *deployment*

Les instructions identifiées ci-dessous (imagePullSecrets) doivent être ajoutées dans votre *deployment*:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "outil-exemples.fullname" . }}
  [...]
spec:
  template:
    metadata:
      [...]
    spec:
      containers:
        - name: {{ .Chart.Name }}
        [...]
      imagePullSecrets:         # <= Ici
        - name: dockerhub       # <= et Là
```

#### Vérification

Après avoir créé le *secret* et modifié votre *deployment* vous pouvez vérifier que tout est en ordre avec les commandes suivantes:

* Vérifier que le *secret* a bien été créé:

```bash
$ kubectl --namespace <monnamespace> get secret dockerhub
NAME        TYPE                             DATA   AGE
dockerhub   kubernetes.io/dockerconfigjson   1      18h
```

* Vérifier que le *deployment* contient bien les instructions d'authenfication au registre
```bash
$ kubectl --namespace <monnamespace> get deploy <mondeploy> -o yaml | grep -A 1 imagePullSecrets
      imagePullSecrets:
      - name: dockerhub
```

Vous pouvez également vérifier le manifest directement depuis le [*dashboard Kubernetes*](https://wiki.uqam.ca/display/SI/Authentification+et+kubeconfig#Authentificationetkubeconfig-Version1.12(actuelle))