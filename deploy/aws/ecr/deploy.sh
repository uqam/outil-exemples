#!/bin/bash


echo "## Deploy ECR registry ##"

echo "*** Creating user infra-ecr ***"
aws iam create-user \
  --user-name infra-ecr \
  --tags \
  Key=uqam:responsable,Value=croset.remi@uqam.ca \
  Key=uqam:nuke,Value=N \
  Key=uqam:cmdb,Value=NA \
  Key=uqam:env,Value=dev \
  Key=uqam:application,Value=NA \
  Key=uqam:equipe,Value=si

aws iam put-user-policy \
  --user-name infra-ecr \
  --policy-name infra-ecr \
  --policy-document file://user-policy.json

aws iam create-access-key --user-name infra-ecr --output json > /tmp/infra-ecr.accesskey.json

  echo "#########################"