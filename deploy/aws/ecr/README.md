# AWS - Elastic Container Registry (ECR)

Les service ECR d'AWS remplit les mêmes fonctions que le registre *Docker Hub* précédemment utilisé à l'UQAM.
Il est désormais le registre recommandé pour déposer les images des applications conteneurisées.

Les avantages par rapport à *Docker Hub*:

* Pas de limitations sur les *pull* (Téléchargement des images)
* Sécurité accrue
  * Scan de sécurité possibles
  * Nombre de dépôts privés illimités


**Table des matières:**

[TOC]

## Pré-requis

### Compte de service
Un compte de service est nécessaire pour déposer les images depuis votre *pipeline Jenkins*.

⚠ Il existe déjà un compte ``infra-ecr`` qui a été créé sur le compte ``SI Dev UQAM`` avec les droits permettant la gestion des registres *ECR*. Il est configuré dans [Jenkins](https://jenkins.dev.uqam.ca) et peut-être utilisé directement dans vos pipelines.

Cette documentation sert à reproduire les étapes de configurations pour un autre compte AWS.

* Créer votre compte de service

```bash
# Création du compte avec les balises UQAM
aws iam create-user \
  --user-name infra-ecr \
  --tags \
  Key=uqam:responsable,Value=croset.remi@uqam.ca \
  Key=uqam:nuke,Value=N \
  Key=uqam:cmdb,Value=NA \
  Key=uqam:env,Value=dev \
  Key=uqam:application-id,Value=NA \
  Key=uqam:equipe,Value=si

# Association des droits au comptes
aws iam put-user-policy \
  --user-name infra-ecr \
  --policy-name infra-ecr \
  --policy-document file://user-policy.json
```

Le fichier ``user-policy.json`` se trouve dans ce dépôt (*deploy/aws/ecr/user-policy.json*).

**Note**: Vous devez disposer des autorisations pour créer un compte de service. Pour les demander, faites demande Jira. (À cette heure c'est Frédéric Bergeron )

* Récupérer les clés d'accès

```bash
aws iam create-access-key --user-name infra-ecr --output json > /tmp/infra-ecr.accesskey.json
```

* Créer des identifiants dans [Jenkins](https://jenkins.dev.uqam.ca/credentials/store/system/domain/_/) pour permettre l'accès au compte de service depuis les *pipelines*

Le *credential* doit être du type ``AWS Credentials`` pour être traité convenablement par la suite.

### Création du registre

* Vérification de l'existence du registre
```bash
aws ecr describe-repositories --repository-names outil-exemples
```

* Création
```bash
aws ecr create-repository --repository-name outil-exemples \
  --image-scanning-configuration scanOnPush=True \
  --tags Key=uqam:responsable,Value=croset.remi@uqam.ca \
         Key=uqam:nuke,Value=N \
         Key=uqam:cmdb,Value=NA \
         Key=uqam:env,Value=dev \
         Key=uqam:application-id,Value=NA \
         Key=uqam:equipe,Value=si
```

## Utilisation et configuration

Pour utiliser les registres ECR dans votre pipeline existant vous devez :

* Intégrer les stages définis dans le fichier [Jenkinsfile.aws.ecr](../../../pipeline/Jenkinsfile.aws.ecr)
* Ajouter les paramètres de configuration identifiés par la balise *#ECR* dans le fichier [Jenkins.params](../../../pipeline/Jenkins.params)

